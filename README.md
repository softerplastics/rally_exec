# rally_exec

git repository for a GameMaker:Studio project named Conventional Motorsports. We assume no liability and offer no warranty, to the extent that we are able to do so.

This is build 00809. It only runs on Windows (for now).

SETUP

Uninstall any earlier versions of the game that you have, then run the installer.

RULES

Players control a team of cars racing around a randomly-generated track consisting of a number of checkpoints. 
The checkpoints are marked by a line of flags and, when passed in a clockwise order, form a roughly circular shape. 

Cars are grouped into "simultaneous heats" which start at different positions around the map. 
Each heat consists of a car for each player.

When a car passes each checkpoint in order and returns to the checkpoint where it began, its owner receives points equal to the number of cars in that heat which are yet to finish. The player with the most points at the end of the game is the winner.

CONTROLLING YOUR CARS

At the start of a turn, each car moves straight ahead a number of tiles equal to their speed. 
Cars then activate one at a time, according to their heat and their distance from the next checkpoint. 

During each activation a car: 

1. executes an action

2. moves forward at its speed, 

3. executes another action.

An action is either: 

1. accelerating (speed +1),

2. braking (speed -2), 

3. turning, 

4. doing nothing (coasting).

Turning the car while its speed is an even number also causes it to slow down (speed -1).
Collisions with other cars or objects on the course may changes the cars speed or direction.
Collisions also disorient a car's driver, temporarily reducing their ability to control the vehicle. A bumped driver will coast instead of performing the intended action. 
Severe collisions may cost the driver several consecutive actions, causing them to careen out of control for one or more turns.

UI

Cars are coloured according to the playerer controlling them. The car with a blue arrow above it is the one currently moving.

The black line indicates course around the checkpoints cars should be attempting to follow.

Flags indicate the tiles which comprise a checkpoint.

Blue lines indicate the next checkpoint each car must cross.
Red lines indicate the final checkpoint for each car.

The faint grey car ahead of the current car shows where it will be at the start of its next turn IF it executes its current orders.
The grey line emitted by this car shows where the car will need to go from there.

Barrels are obstacles which will bump the driver and bring the car to a dead stop if collided with.
Fences bump the driver and reduce speed (speed -1) as the car crashes through them (even if other cars have gone through it already).
Sand traps reduce speed (speed -1) but do not bump the driver.

CONTROLS
vehicle controls

+ w - select accelerate (increase speed by 1).
+ a - select turn 60 degrees left (reduce speed by 1 if it is an even number).
+ d - select turn 60 degrees right (reduce speed by 1 if it is an even number).
+ s - select brake (reduce speed by 2).
+ e - select coast (maintain current speed and direction).
+ shift - toggle between selecting the car's 1st action and 2nd action.
+ space - drive (execute 1st action, move forward a number of tiles equal to your speed, execute 2nd action).

UI controls

+ backspace - toggle between the standard view and a map of the course (hack-y, looks ugly as hell).
+ ctrl - lock or unlock the camera from the car that is currently moving.
+ mouse around - pan the camera in standard view. mouseover cars to see some more information about them. Mouseover button cluster in top-righthand corner for tooltips
+ hold LMB - increase pan speed.

DEFAULT RACE SETTINGS
The default settings for a race are read from a file called level.ini. Editing this file will allow the the game to default to your preferred settings, so that it isn't necessary to use the SETUP GAME option in the main menu each time.
After installing, find and open level.ini. It's usually in the same folder as the game rally.exe file. Notepad should do the trick.
Remember to save your changes.

KNOWN BUGS AND ISSUES

+ Ghost cars do not always adjust position immediately to players keyboard input
+ Speed and timing of some animations is strange.
+ Placement of some buttons in the settings menu is off.
+ Very low values for "terrain smoothness" and "checkpoint spacing approximation" can drastically increase the time it takes to generate new matches.
+ AI can get confused by large areas of blocking terrain and stuck.

RECENT CHANGES
build 0809
+ camera panning now only applies at the border of the map, and only while the camera is unlocked. look for the green bars
+ a new button toggles camera lock, and indicates lock state.
+ cars move smoothly between locations
+ ghost cars now show have mouseover text and breadcrumbs.
+ team setup option in menu allows you to name your team and individual cars. The first human player uses team 1, the second uses team 2, etc. these names are stored in teamnames.ini.
+ non-functional credits option in main menu.

build 00638

+ standard view panning with mouse, snap-to with Ctrl
+ terrain placed in clusters on the map. lower the "terrain smoothness" setting to increase the size and number of terrain clusters.
+ bumps now override move selection choices rather than the toggle between 1st and 2nd moves

build 00485

+ fixed bugged display of buttons in menu
+ mouseover text for the ingame buttons

build 00484

+ substantial changes to the GUI and menu system
+ race options can be set ingame using the SETUP GAME button in the main menu.
+ level.ini is now only used to provide default settings and en/disable a few debug option
+ 'autostart' variable in level.ini bypasses the main menu and starts a game with the default settings

build 00288

+ timeouts are now controlled by the 'timeout' and 'timeout_turns' variables in level.ini. set 'timeout' to 0 to disable it.

build 00286

+ modified placement of checkpoints using Lloyd relaxation. as this is more complex than the previous method, loading times will increase compared to earlier versions. if these times are too long try increasing the denom_ctrl variable in level.ini.
+ larger maps. a "camera" now centers on the active car.
+ pressing backspace shows or hides the entire map.

build 00176

+ made gallant smarter. it now avoids collisions with obstacles and other cars where a suitable alternative presents itself.

build 00161

+ reduces use of level.ini and makes the process of configuring races less messy. values from level.ini are now read into memory at the start of each map. this means that you can set up your next game while the current one is underway.
+ improved the left-hand GUI panel
+ each car leaves a trail of coloured breadcrumbs as it moves. these breadcrumbs are displayed whenever the car or another car from the same heat is active.
+ to reduce visual clutter, red and blue lines indicating the next and final checkpoints for a car now only display during that car's activation.

build 00129

+ reading from level.ini has been fixed. if values in the file would cause a problem (e.g. requiring more cars than there is room for on the starting line) a "safe version" of the .ini is created elsewhere and read from. The safe version will be cleared at the start of the next map, allowing the level.ini to be rechecked.

build 00102

+ races now continue until all cars have passed the appropriate finish line
+ turn ordering has been altered to work as originally intended. Priority is as follows: Heat, Number of uncompleted checkpoints, distance to next checkpoint
+ car number and team names are now displayed in mouseover boxes. Car numbers are determined arbitrarily, while team names are "<random NATO letter> <AI type>" for computer players or "H.Sapiens" for human players

build 00090

+ level.ini is checked for bad values before being used to generate a new map

SUGGESTIONS, QUESTIONS AND BUG REPORTS

email: softerplastics@mail.com

twitter: @lakemistake

Happy Driving!
============